
;;; Start up file for EECS 325
;;; Platform: Allegro Common Lisp on Unix/MacOS/Windows
;;;
;;; Place this file in your home directory
;;;   Unix/MacOS: ~/clinit.cl
;;;   Windows XP and up: %USERPROFILE%\clinit.cl


;;; Turn off the list printing limits.

(setq tpl:*print-length* nil)
(setq tpl:*print-level* nil)

;;; Load Quicklisp

#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))


;;; Load CS325.LISP, and set the initial package



(eval-when (:compile-toplevel :load-toplevel :execute)
 
  (ql:quickload "cs325")

  (when (find-package :cs325-user)
    (tpl:setq-default *package* (find-package :cs325-user))
    (rplacd (assoc 'tpl::*saved-package*
                   tpl:*default-lisp-listener-bindings*)
            'common-lisp:*package*)))
