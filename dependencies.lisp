
;;; 09-17-14 Changed SBCL to load hunchentoot, dropped webactions [CKR]
;;; 09-05-14 Created file [CKR]

#+allegro (require "aserve")
#+lispworks (ql:quickload "aserve")
#+sbcl (ql:quickload "hunchentoot")

(ql:quickload "drakma")
