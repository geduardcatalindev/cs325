;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Clyde the white elephant
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Clyde is white and got sick eating peanuts.

(mammal (animal) (num-hearts 1) (num-eyes 2))
(elephant (mammal) (color gray))
(pig (mammal) (color pink))
(clyde-1 (elephant) (name "Clyde") (age 15) (color white))
(jumbo-1 (elephant) (name "Jumbo") (age 40))
(wilbur-1 (pig) (name "Wilbur") (age 2))

(nuts (plant food))
(peanuts (nuts))
(peanuts-1 (peanuts))

(ingest (action))
(ingest-event (event) (actor animal) (action ingest) (object food))

(event-1 (ingest-event)(actor clyde-1) (object peanuts-1))
(state-1 (state) (actor clyde-1) (state nauseous))

(causal-1 (causal) (cause event-1) (result state-1))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;  Inheritance examples
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(dog (animal) (legs 4) (skin furred))
(buddy (chihuahua pet) (age 7))
(chihuahua (dog small-thing) (brain nil))
(pet (animal) (owner human))
(animal (living-thing))
(living-thing (thing))
(small-thing (thing) (size small))
(tweety (canary) (owner granny))
(granny (human))
(human (animal))
(canary (bird small-thing) (color yellow))
(bird (animal) (can-fly t))
(penguin (bird) (can-fly nil) (can-swim t))
(willy (penguin))

;;; Pedalo example

(boat (thing))
(day-boat (boat) (navzone 5))
(wheel-boat (boat) (navzone 100))
(engineless-boat (day-boat))
(small-multi-hull-boat (day-boat))
(pedal-wheel-boat (engineless-boat wheel-boat))
(small-catamaran (small-multi-hull-boat))
(pedalo (pedal-wheel-boat small-catamaran))

