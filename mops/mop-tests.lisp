(defpackage #:mop-tests
  (:use #:common-lisp #:lisp-unit #:mops))

(in-package #:mop-tests)

(defconstant *mop-file*
  (merge-pathnames "~/quicklisp/local-projects/cs325/mops/mop-examples.txt"))

;;; Existing MOP functions

(define-test isa-p
  (unless (kb-source)
    (load-kb *mop-file*))
  (assert-true (isa-p 'clyde-1 'animal))
  (assert-true (isa-p 'buddy 'animal))
  (assert-true (isa-p 'buddy 'small-thing))
  (assert-false (isa-p 'clyde-1 'chihuahua))
  (assert-false (isa-p 'elephant 'clyde-1))
  )

(define-test get-filler
  (unless (kb-source)
    (load-kb *mop-file*))
  (assert-equal 'white (get-filler 'clyde-1 'color))
  (assert-equal 'gray (get-filler 'elephant 'color))
  (assert-equal nil (get-filler 'willy 'can-fly))
  (assert-equal t (get-filler 'willy 'can-swim))
  (assert-equal t (get-filler 'tweety 'can-fly))
  (assert-equal nil (get-filler 'tweety 'can-swim))
  )

;;; Tests for exercises

(define-test has-slots-p
  (unless (kb-source)
    (load-kb *mop-file*))
  (assert-true (has-slots-p 'jumbo-1 '((color gray) (name "Jumbo"))))
  (assert-false (has-slots-p 'clyde-1 '((color gray))))
  (assert-true (has-slots-p 'event-1 '((actor elephant) (action ingest))))
  (assert-true (has-slots-p 'buddy '((brain nil))))
  (assert-false (has-slots-p 'tweety '((brain nil))))
  )

(define-test pedalo
  ;;; always reload mops to force re-linearization
  (load-kb *mop-file*)
  (assert-equal '5 (get-filler 'pedal-wheel-boat 'navzone))
  (assert-equal '5 (get-filler 'small-catamaran 'navzone))
  (assert-equal '5 (get-filler 'pedalo 'navzone))
  )  
 
(define-test mop-search
  (unless (kb-source)
    (load-kb *mop-file*))
  (assert-equal '(canary) (mop-search 'bird '((color yellow))))
  (assert-equal '(tweety) (mop-search 'bird '((owner granny))))
  (assert-equal '(tweety) (mop-search 'bird '((color yellow) (owner granny))))
  (assert-equal '(buddy) (mop-search 'animal '((age 7))))
  (assert-equal '(chihuahua) (mop-search 'animal '((brain nil))))
  
  (assert-equal nil (mop-search 'event '((actor elephant))))
  (assert-equal '(event-1) (mop-search 'event '((actor clyde-1))))
  )


(define-test dmap
  (unless (kb-source)
    (load-kb *mop-file*))
  (set-phrases 
   '((clyde-1 clyde)
     (peanuts peanuts) 
     (ingest-event (actor) ate (object))
     ))

  ;;; test that simple word goes to concept
  (assert-equal '(clyde-1) (references (dmap '(clyde))))
  ;;; test that word goes to concept with same name without loop
  (assert-equal '(peanuts) (references (dmap '(peanuts))))
  ;;; test that sentence goes to event with given slots
  (assert-equality set-equal '(clyde-1 peanuts event-1) 
                   (references (dmap '(clyde ate some peanuts))))
  )
